#!/bin/bash
####################################################################################################
# Reduce the size of Post-inForm input files.
# *******************************************
# The script loads all '*_cell_seg_data.txt' and '*_tissue_seg_data_summary.txt' files and deletes
# all unnecessary columns in them.
# This is done to increase the speed and reduce the memory footprint of the analysis in R,
# because R is slow to read large files and uses a lot of memory when opening large tables.
#
#  -> inputDir       : directory containing the data to process.
#  -> cellCompartment: cell compartement that should be considered in the analysis.
#                      e.g. 'Nucleus', 'Membrane', 'Cytoplasm', 'Entire.Cell'.
#  -> verbose        : 0 = only error message are printed. 1 = progress message are displayed.
#
# Input data check and setup.
# **************************
inputDir="$1"
outputDir="$2"
cellCompartment="$3"
verbose="$4"
[[ -z "$inputDir" ]] && echo "### ERROR: input parameter 'inputDir' is missing." && exit 1
[[ -z "$outputDir" ]] && echo "### ERROR: input parameter 'outputDir' is missing." && exit 1
[[ -z "$cellCompartment" ]] && echo "### ERROR: input parameter 'cellCompartment' is missing." \
                            && exit 1
[[ -z $verbose ]] && verbose=0
[[ $cellCompartment == "Entire.Cell" ]] && cellCompartment="Entire Cell"

# Display progress message.
if [[ $verbose -eq 1 ]]; then
	echo "################################################################################"
	echo "### Starting data reduction script."
	echo "### Input values:"
	echo "###  -> input dir       : $inputDir"
    echo "###  -> input dir       : $outputDir"
	echo "###  -> cell compartment: $cellCompartment"
	echo "### "
fi

# Create output directory if needed.
[[ "$inputDir" == "$outputDir" ]] && echo "### ERROR: 'inputDir' and 'outputDir' must differ." \
                                  && exit 1
[[ ! -d "$outputDir" ]] && mkdir -p "$outputDir"


# Reduce cell segmentation (*_cell_seg_data.txt) and
# tissue surface (*_tissue_seg_data_summary.txt) files
# ****************************************************
for fileExtension in '_cell_seg_data.txt' '_tissue_seg_data_summary.txt'; do
    [[ $verbose -eq 1 ]] && echo "### Reduce ${fileExtension} files:"

    # Retrieve all cell/tissue files, except "rejected" files. If "merge" file are present, keep
    # only the "merge" files.
    if [[ $( find "$inputDir" -type f -name "*_merge${fileExtension}" | wc -l ) -gt 0 ]]; then
        fileList=($(find "$inputDir" -type f -name "*_merge${fileExtension}" -a ! -name "*_rejected_*"))
    else
        fileList=($(find "$inputDir" -type f -name "*${fileExtension}" -a ! -name "*_rejected_*"))
    fi

    # For each input file, keep only the columns that are needed for the analysis.
    for fileName in "${fileList[@]}"; do
        [[ $verbose -eq 1 ]] && echo "###  -> ${fileName}"

        # Get list of columns to keep.
        if [[ $fileExtension == '_cell_seg_data.txt' ]]; then
            colsToKeep=$(head -n1 "$fileName" | tr '\t' '\n' | \
                         grep -i -n "^Sample Name$\|^Tissue Category$\|^Phenotype$\|^Cell ID$\|^Cell X Position$\|^Cell Y Position$\|^Annotation ID\|^Confidence\|^${cellCompartment} .* Mean " | \
                         cut -f1 --delimiter=':')
        else
            colsToKeep=$(head -n1 "$fileName" | tr '\t' '\n' | \
                         grep -n "^Sample Name$\|^Tissue Category$\|^Region Area\|^Annotation ID" | \
                         cut -f1 --delimiter=':')
        fi
        [[ -z $colsToKeep ]] && echo "### ERROR: no columns to keep found in [$fileName]" && exit 1

        # Save copy of file with only the selected columns to output directory.
        cut -f$( echo $colsToKeep | tr ' ' ',' ) "$fileName" > "$outputDir/$( basename $fileName )"
        unset colsToKeep
    done

    [[ $verbose -eq 1 ]] && echo -e "###  -> completed. \n### "
    unset fileList fileName
done

if [[ $verbose -eq 1 ]]; then
    echo "### Data reduction completed."
    echo "################################################################################"
fi
exit 0
####################################################################################################
